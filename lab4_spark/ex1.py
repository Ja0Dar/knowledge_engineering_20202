from pyspark import SparkContext
import random
import os


os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk"


sc = SparkContext("local", "lab4-spark")

SAMPLES = 10000000


def f(x):
    return x ** 2 + 1


def is_in_area(nothing):
    return f(random.random() * 2.0) - random.random() * 5.0 > 0


count = sc.parallelize(range(SAMPLES)).filter(is_in_area).count()

result = 10.0 * count / SAMPLES

print(f"Result: {result}")
print(f"Error: { abs(result - 14/3) }")

sc.stop()

import functools
from pyspark import SparkContext
import regex
import os


os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk"


def compose(*functions):
    def compose2(f, g):
        return lambda x: f(g(x))
    return functools.reduce(compose2, functions, lambda x: x)


sc = SparkContext("local", "lab4-spark")


bible = sc.textFile("the_king_james_bible.txt")
communist = sc.textFile("the_communist_manifesto.txt")

ws = r"\s+"
non_letter_pattern = r"[^\p{L} ]"


def normalize_whitespace(s: str) -> str:
    return regex.sub(ws, " ", s)


def clean_chars(s: str) -> str:
    return regex.sub(non_letter_pattern, "", s).lower()


def do_things(rdd):
    return (
        rdd
        .flatMap(lambda s: compose(normalize_whitespace, clean_chars)(s).split(" "))
        .map(lambda w: (w, 1))
        .reduceByKey(lambda x, y: x + y)
        .sortBy(lambda x: -x[1]).filter(lambda x: len(x[0]) > 0)
    )


bible_counts = do_things(bible)
communitst_counts = do_things(communist)


def is_way_more_popular(a: int, b: int, a_count: int, b_count: int, factor: int):
    a = list(a)
    b = list(b)

    a_non_empty = len(a) != 0
    b_non_empty = len(b) != 0

    return a_non_empty and b_non_empty and a[0] * b_count >= factor * b[0] * a_count


def many_way_more_popular(a, b, n=20):
    a_count = a.map(lambda x: x[1]).sum()
    b_count = b.map(lambda x: x[1]).sum()

    return (
        a.cogroup(b)
        .filter(lambda x: is_way_more_popular(x[1][0], x[1][1], a_count, b_count, factor=5))
        .map(lambda x: x[0])
        .take(n)
    )


print("Bbible popular words")
print(many_way_more_popular(bible_counts, communitst_counts))

print("Communist popular words")
print(many_way_more_popular(communitst_counts, bible_counts))

# results:
# Bbible popular words
# ['he', 'i', 'his', 'them', 'said', 'upon', 'came', 'before', 'let', 'make', 'say', 'give', 'pass', 'cast', 'midst', 'head', 'shall', 'lord', 'him', 'people']
# Communist popular words
# ['more', 'conditions', 'only', 'means', 'labour', 'form', 'state', 'working', 'new', 'longer', 'common', 'country', 'ruling', 'middle', 'family', 'countries', 'must', 'forces', 'just', 'free']

sc.stop()

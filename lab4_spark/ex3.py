import functools
from pyspark import SparkContext
import os

import matplotlib.pyplot as plt

from pyspark.ml.feature import RegexTokenizer, StopWordsRemover, Word2Vec
from pyspark.sql import SQLContext, Row


os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk"


def andThen(*functions):
    def compose2(f, g):
        return lambda x: f(g(x))

    return functools.reduce(compose2, reversed(functions), lambda x: x)


Schema = Row("sentences", "source")
sc = SparkContext("local", "lab4-spark")
sqlContext = SQLContext(sc)


def sentencize(rdd, source):
    return (
        rdd.flatMap(lambda x: x.split("."))
        .filter(lambda x: len(x) > 0)
        .map(lambda x: Schema(x, source))
    )


bible = sqlContext.createDataFrame(
    sentencize(sc.textFile("the_king_james_bible.txt"), "bible")
)
communist = sqlContext.createDataFrame(
    sentencize(sc.textFile("the_communist_manifesto.txt"), "communist")
)

tokenizer = RegexTokenizer(inputCol="sentences", outputCol="words", pattern="\\W")
remover = StopWordsRemover(inputCol="words", outputCol="filtered")
w2v = Word2Vec(vectorSize=2, minCount=0, inputCol="filtered", outputCol="w2v")

df = andThen(
    tokenizer.transform,
    remover.transform,
    lambda d: w2v.fit(d).transform(d))(bible.union(communist))


def plot_data(df, source):
    rows = df.filter(df.source == source).select("w2v").collect()
    x, y = zip(*[(r[0][0], r[0][1]) for r in rows])
    return list(x), list(y)


bx, by = plot_data(df, "bible")
cx, cy = plot_data(df, "communist")
plt.scatter(bx, by, c="b")
plt.scatter(cx, cy, c="r")
plt.show()
sc.stop()

#result in ex3.png

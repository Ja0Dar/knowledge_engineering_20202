from utils import binary2neg_boolean
import numpy as np
from sklearn.svm import OneClassSVM


def detect(train: np.ndarray, test: np.ndarray) -> list:
    svm = OneClassSVM(kernel='rbf', gamma=0.45, nu=0.1).fit(train)
    return binary2neg_boolean(svm.predict(test))

import numpy as np


def detect(train: np.ndarray, test: np.ndarray, z_score: float = 2.66) -> np.ndarray:
    mean, std = np.mean(train), np.std(train)
    lower_bound = mean - z_score * std
    upper_bound = mean + z_score * std
    return ((test < lower_bound) | (upper_bound < test)).astype(int)

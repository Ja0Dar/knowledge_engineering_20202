from sklearn.svm import OneClassSVM
from sklearn.covariance import EllipticEnvelope
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from utils import binary2neg_boolean
import numpy as np

SEED = 1


def detect_cov(data: np.ndarray, outliers_fraction: float) -> list:
    ee = EllipticEnvelope(contamination=outliers_fraction, random_state=SEED).fit(data)
    return binary2neg_boolean(ee.predict(data))


def detect_ocsvm(data: np.ndarray, outliers_fraction: float) -> list:
    svm = OneClassSVM(nu=outliers_fraction, random_state=SEED).fit(data)
    return binary2neg_boolean(svm.predict(data))


def detect_iforest(data: np.ndarray, outliers_fraction: float) -> list:
    forrest = IsolationForest(contamination=outliers_fraction, random_state=SEED).fit(
        data
    )
    return binary2neg_boolean(forrest.predict(data))


def detect_lof(data: np.ndarray, outliers_fraction: float) -> list:
    kwargs = {
        "n_neighbors": 300,
        "metric": "manhattan",
        "leaf_size": 10,
        "algorithm": "ball_tree"
    }
    return binary2neg_boolean(
        LocalOutlierFactor(contamination=outliers_fraction, **kwargs).fit_predict(data)
    )

import numpy as np
from sklearn.covariance import MinCovDet
from scipy.spatial import distance


def detect(train: np.ndarray, test: np.ndarray) -> np.ndarray:

    minCov = MinCovDet(random_state=0).fit(train)
    train_cov_inv = np.linalg.inv(minCov.covariance_)

    mean = train.mean(axis=0)

    train_dists = [
        distance.mahalanobis(train[i, :], mean, train_cov_inv)
        for i in range(train.shape[0])
    ]

    max_dist = np.max(train_dists)

    test_dists = np.array(
        [
            distance.mahalanobis(test[i, :], mean, train_cov_inv)
            for i in range(test.shape[0])
        ]
    )

    # Could be tweaked by multiplying max_dist by some threshold
    return (test_dists > (max_dist * 0.69)).astype(int)

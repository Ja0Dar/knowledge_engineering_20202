#!/usr/bin/python
import math
import numpy as np

from functional import pseq, seq
from typing import List, Tuple
from tqdm import tqdm
import sys
import random
from itertools import product
import gym

MAX_REWARD_THRESHOLD = 480


class QLearner:
    def __init__(
        self, buckets, eps, lr, discount_factor, render, print_it, decay, use_sarsa
    ):
        self.param_id = f"{lr}|{eps}|{discount_factor}|{buckets[0]}|{decay}"
        self.environment = gym.make("CartPole-v1")
        self.attempt_no = 1
        self.eps = eps
        self.decay = decay
        self.use_sarsa = use_sarsa
        self.lr = lr
        self.render = render
        self.discount_factor = discount_factor
        self.upper_bounds = [
            self.environment.observation_space.high[0],  # x
            0.5,  # velocity x
            self.environment.observation_space.high[2],  # wychylenie
            math.radians(50),  # predkosc katowa
        ]
        self.print_it = print_it
        self.lower_bounds = [
            self.environment.observation_space.low[0],
            -0.5,
            self.environment.observation_space.low[2],
            -math.radians(50),
        ]
        self.buckets = buckets

        self.Q = {}

    def learn(self, max_attempts) -> int:
        rewards = []

        for attempt_nr in range(max_attempts):
            reward_sum = self.attempt_sarsa() if self.use_sarsa else self.attempt()
            if reward_sum > MAX_REWARD_THRESHOLD:
                rewards.append(reward_sum)

            if attempt_nr % 100 == 0:
                self.lr -= self.decay * self.lr
                self.eps -= self.decay * self.eps

            if self.print_it:
                print(f"{self.param_id},{attempt_nr},{reward_sum}")

        cutoff = int(len(rewards) / 3)
        if len(rewards) == 0:
            return 0
        elif len(rewards[-cutoff:]) == 0:
            return sum(rewards) / len(rewards)
        else:
            return sum(rewards[-cutoff:]) / len(rewards[-cutoff:])

    def attempt(self):
        observation = self.discretise(self.environment.reset())
        done = False
        reward_sum = 0.0

        i = 0
        while not done:
            i += 1
            # Show
            if self.render and i > 600:
                self.environment.render()

            action = self.pick_action(observation)
            new_observation, reward, done, info = self.environment.step(action)
            new_observation = self.discretise(new_observation)
            self.update_knowledge(action, observation, new_observation, reward)
            observation = new_observation
            reward_sum += reward
        self.attempt_no += 1
        return reward_sum

    def attempt_sarsa(self):
        observation = self.discretise(self.environment.reset())
        done = False
        reward_sum = 0.0

        i = 0
        action = self.pick_action(observation)
        while not done:
            i += 1
            # Show
            if self.render and i > 600:
                self.environment.render()

            new_observation, reward, done, info = self.environment.step(action)
            new_observation = self.discretise(new_observation)

            new_action = self.pick_action(observation)
            self.update_knowledge_sarsa(
                action, observation, new_observation, reward, new_action
            )

            action = new_action
            observation = new_observation
            reward_sum += reward
        self.attempt_no += 1
        return reward_sum

    def bound(self, upper, lower, buckets, observation):
        span = upper - lower
        o = observation - lower
        return int((o / span) * buckets)

    def discretise(self, observation):
        return tuple(
            [
                self.bound(
                    self.upper_bounds[i], self.lower_bounds[i], self.buckets[i], value
                )
                for i, value in enumerate(observation)
            ]
        )

    def pick_action(self, observation):
        if random.random() < self.eps:
            return random.choice([0, 1])
        elif (observation, 1) in self.Q and (observation, 0) in self.Q:
            if self.Q[(observation, 1)] > self.Q[(observation, 0)]:
                return 1
            else:
                return 0
        else:
            return self.environment.action_space.sample()

    def update_knowledge(self, action, observation, new_observation, reward):

        mymax = max(
            self.Q.get((new_observation, 0), 0), self.Q.get((new_observation, 1), 0)
        )

        new_reward = (1.0 - self.lr) * self.Q.get(
            (observation, action), 0.0
        ) + self.lr * (reward + self.discount_factor * mymax)
        self.Q[(observation, action)] = new_reward

    def update_knowledge_sarsa(self, action, observation, new_observation, reward, new_action):
        next_action_reward = self.Q.get(new_observation, new_action)
        learned_value = reward + self.discount_factor * next_action_reward
        self.Q[(observation, action)] = (1.0 - self.lr) * self.Q.get(
            (observation, action), 0.0
        ) + self.lr * learned_value


def find_best_params(sarsa: bool):

    float_range = np.arange(0.3, 1.0, step=0.1, dtype=float)
    decay_range = np.arange(0.25, 0.45, 0.05)

    def all():
        return range(3, 6)

    buckets = [
        # [x, v, ang, ang_v] for v in all() for x in all() for ang in all() for ang_v in all()
        [v, v, v, v]
        for v in all()
    ]

    def get_reward(params: Tuple[float, float, float, List[int], float]):
        lr, df, eps, buck, decay = params
        return QLearner(buck, eps, lr, df, False, True, decay, sarsa).learn(1000)

    # permutation_count = len(float_range) * len(float_range) * len(float_range) * len(buckets) * len(decay_range)
    # print(permutation_count, file=sys.stderr)
    # exit(1)

    print("params,iteration,reward")
    res = (
        seq(tqdm(product(float_range, float_range, float_range, buckets, decay_range)))
        .map(lambda params: (get_reward(params), params))
        .to_list()
    )  # .order_by(lambda x: -x[0]).take(10).to_list()


if __name__ == "__main__":

    _, lr, df, eps, buck, decay = sys.argv
    print("params,iteration,reward")
    QLearner([int(buck)] * 4, float(eps), float(lr), float(df), False, True, float(decay), use_sarsa=True).learn(50000)

    # find_best_params(use_sarsa=True)

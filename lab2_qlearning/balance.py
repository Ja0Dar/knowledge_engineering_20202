#!/usr/bin/python
import math
import numpy as np

from functional import pseq, seq
from typing import List, Tuple
from tqdm import tqdm
import sys
import random
from itertools import product
import gym


class QLearner:
    def __init__(self, buckets, eps, lr, discount_factor, print_it, decay, use_sarsa):
        self.param_id = f"{lr}|{eps}|{discount_factor}|{buckets[0]}|{decay}"
        self.environment = gym.make("CartPole-v1")
        self.attempt_no = 1
        self.eps = eps
        self.decay = decay
        self.use_sarsa = use_sarsa
        self.lr = lr
        self.discount_factor = discount_factor
        self.upper_bounds = [
            self.environment.observation_space.high[0],  # x
            0.5,  # velocity x
            self.environment.observation_space.high[2],  # wychylenie
            math.radians(50),  # predkosc katowa
        ]
        self.print_it = print_it
        self.lower_bounds = [
            self.environment.observation_space.low[0],
            -0.5,
            self.environment.observation_space.low[2],
            -math.radians(50),
        ]
        self.buckets = buckets

        self.Q = {}

    def learn(self, max_attempts) -> int:

        for attempt_nr in range(max_attempts):
            reward_sum = self.attempt_sarsa() if self.use_sarsa else self.attempt()

            if attempt_nr % 100 == 0:
                self.lr -= self.decay * self.lr
                self.eps -= self.decay * self.eps

            if self.print_it:
                print(f"{self.param_id},{attempt_nr},{reward_sum}")

    def attempt(self):
        observation = self.discretise(self.environment.reset())
        done = False
        reward_sum = 0.0

        i = 0
        while not done:
            i += 1
            # if self.attempt_no > (10000 - 5):
            #     self.environment.render()
            action = self.pick_action(observation)
            new_observation, reward, done, info = self.environment.step(action)
            new_observation = self.discretise(new_observation)

            # TODO:bcm  wtf
            # if done and reward_sum < 499:
            #     reward = -1  # -1000
            # else:
            #     reward = 0  # 0.0
            self.update_knowledge(action, observation, new_observation, reward)
            observation = new_observation

            # reward = 1  # TODO:bcm
            reward_sum += reward
        self.attempt_no += 1
        return reward_sum

    def attempt_sarsa(self):
        observation = self.discretise(self.environment.reset())
        done = False
        reward_sum = 0.0

        i = 0
        action = self.pick_action(observation)
        while not done:
            i += 1
            new_observation, reward, done, info = self.environment.step(action)
            new_observation = self.discretise(new_observation)

            new_action = self.pick_action(observation)
            self.update_knowledge_sarsa(
                action, observation, new_observation, reward, new_action
            )

            action = new_action
            observation = new_observation
            reward_sum += reward
        self.attempt_no += 1
        return reward_sum

    def bound(self, upper, lower, buckets, observation):
        span = upper - lower
        o = observation - lower
        return int((o / span) * buckets)

    def discretise(self, observation):
        return tuple(
            [
                self.bound(
                    self.upper_bounds[i], self.lower_bounds[i], self.buckets[i], value
                )
                for i, value in enumerate(observation)
            ]
        )

    def pick_action(self, observation):
        if random.random() < self.eps:
            return random.choice([0, 1])
        elif (observation, 1) in self.Q and (observation, 0) in self.Q:
            if self.Q[(observation, 1)] > self.Q[(observation, 0)]:
                return 1
            else:
                return 0
        else:
            return self.environment.action_space.sample()

    def update_knowledge(self, action, observation, new_observation, reward):

        mymax = max(
            self.Q.get((new_observation, 0), 0), self.Q.get((new_observation, 1), 0)
        )

        new_reward = (1.0 - self.lr) * self.Q.get(
            (observation, action), 0.0
        ) + self.lr * (reward + self.discount_factor * mymax)
        self.Q[(observation, action)] = new_reward

    def update_knowledge_sarsa(
        self, action, observation, new_observation, reward, new_action
    ):
        next_action_reward = self.Q.get(new_observation, new_action)
        learned_value = reward + self.discount_factor * next_action_reward
        self.Q[(observation, action)] = (1.0 - self.lr) * self.Q.get(
            (observation, action), 0.0
        ) + self.lr * learned_value


def find_best_params(use_sarsa: bool):

    # float_range = [0.0, 0.01, 0.05, 0.1, 0.5, 0.7, 0.9, 0.95]
    # eps_range = [0.1, 0.5, 0.7, 0.9]
    # decay_range = [0, 0.3]
    # bucket_range = [[5] * 4]  # [[v] for v in range(5, 7)]

    float_range = np.arange(0.1, 1, step=0.1, dtype=float)
    decay_range = np.arange(0.0, 0.3, 0.05)
    eps_range = float_range
    bucket_range = [[3] * 4, [4] * 4]

    def get_reward(params: Tuple[float, float, float, List[int], float]):
        lr, df, eps, buck, decay = params
        return QLearner(buck, eps, lr, df, True, decay, use_sarsa=use_sarsa).learn(
            1000
        )

    params = list(product(float_range, float_range, eps_range, bucket_range, decay_range))
    # print(len(params))
    # exit(1)

    print("params,iteration,reward")
    seq(tqdm(params))\
        .for_each(lambda params: (get_reward(params), params))


def find_sarsa():
    find_best_params(use_sarsa=True)


def find_qlearn():
    find_best_params(use_sarsa=False)


def run_sarsa(lr: float, df: float, eps: float, buck: int, decay: float):
    buckets = [int(buck)] * 4

    QLearner(
        buckets, eps, lr, df, True, decay, use_sarsa=True
    ).learn(10000)


def run_qlearn(lr: float, df: float, eps: float, buck: int, decay: float):
    buckets = [int(buck)] * 4

    QLearner(
        buckets, eps, lr, df, True, decay, use_sarsa=False
    ).learn(10000)


if __name__ == "__main__":

    import fire
    fire.Fire({
        "find_qlearn": find_qlearn,
        "find_sarsa": find_sarsa,
        "run_qlearn": run_qlearn,
        "run_sarsa": run_sarsa,

    })

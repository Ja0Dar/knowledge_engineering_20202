import math
import random
import gym
import itertools
from functional import seq


class QLearner:
    def __init__(self, alpha: float, gamma: float, eps: float, sarsa: bool):
        self.param_id = f"{alpha}|{eps}|{gamma}|6"
        self.alpha = alpha
        self.gamma = gamma
        self.step = 0
        self.sarsa = sarsa
        self.eps = eps
        self.Q = {}
        self.environment = gym.make("LunarLander-v2")
        self.upper_bounds = [1.0] * 8
        self.lower_bounds = [-1.0] * 8

        self.alpha_start, self.alpha_end, self.alpha_steps, self.alpha_decay = [0.2, 0.2, 0, 1]
        self.epsilon_start, self.epsilon_end, self.epsilon_steps, self.epsilon_decay = [1.0, 0.05, 1e5, 0.97]
        self.alpha, self.eps = self.alpha_start, self.epsilon_start

    def update_alpha_step(self):
        # Linear decay
        if self.step <= self.alpha_steps and self.alpha_steps > 0:
            self.alpha = self.alpha_start - self.step * (self.alpha_start - self.alpha_end) / self.alpha_steps

    def update_epsilon_step(self):
        # Linear decay
        if self.step <= self.epsilon_steps and self.epsilon_steps > 0:
            self.eps = self.epsilon_start - self.step * (self.epsilon_start - self.epsilon_end) / self.epsilon_steps

    def update_alpha_episode(self):
        # Exponential decay
        if self.step > self.alpha_steps:
            self.alpha *= self.alpha_decay

    def update_epsilon_episode(self):
        # Exponential decay
        if self.step > self.epsilon_steps:
            self.eps *= self.epsilon_decay

    def learn(self, max_attempts):

        for attempt_nr in range(max_attempts):
            reward_sum = self.attempt()
            print(f"{self.param_id},{attempt_nr},{reward_sum}")

    def attempt(self):
        self.update_epsilon_step()

        # Episodic decay (only after linear decay)
        self.update_alpha_episode()
        self.update_epsilon_episode()

        observation = self.discretise(self.environment.reset())
        done = False
        reward_sum = 0.0
        while not done:

            # Update for other steps
            self.update_alpha_step()
            self.update_epsilon_step()

            action = self.pick_action(observation)
            new_observation, reward, done, info = self.environment.step(action)
            new_observation = self.discretise(new_observation)

            # self.update_knowledge(action, observation, new_observation, reward)
            self.update_knowledge_SARSA(action, observation, new_observation, reward)
            # reward = 1
            observation = new_observation
            reward_sum += reward
            self.step += 1
        return reward_sum

    def bound(self, upper, lower, buckets, observation):
        span = upper - lower
        o = observation - lower
        return int((o / span) * buckets)

    def discretise(self, observation):
        buckets = [5, 5, 5, 5, 5, 5, 2, 2]
        return tuple(
            [
                self.bound(
                    self.upper_bounds[i], self.lower_bounds[i], buckets[i], value
                )
                for i, value in enumerate(observation)
            ]
        )

    def pick_action(self, observation):
        if random.random() < self.eps:
            return self.environment.action_space.sample()
        out_action = 0
        min_reward = -100000000
        for i in range(4):
            if min_reward < self.Q.get((observation, i), 0.0):
                min_reward = self.Q.get((observation, i), 0.0)
                out_action = i
        return out_action

    def update_knowledge(self, action, observation, new_observation, reward):

        best_reward = seq([1, 2, 3, 4]).map(lambda a: self.Q.get((new_observation, a), 0.0)).max()

        self.Q[(observation, action)] = (1.0 - self.alpha) * self.Q.get((observation, action), 0) + \
            self.alpha * (reward + self.gamma * best_reward)

    def update_knowledge_SARSA(self, action, observation, new_observation, reward):
        self.Q[(observation, action)] = self.Q.get(
            (observation, action), 0.0) + self.alpha * (
            reward +
            self.gamma *
            self.Q.get((new_observation, self.pick_action(observation)), 0.0) -
            self.Q.get((observation, action), 0.0)
        )


if __name__ == "__main__":

    # E_GREEDY:
    # LEARNING_RATE:
    # DISCOUNT_RATE:

    learner = QLearner(alpha=None, gamma=0.99, eps=None, sarsa=True)
    learner.learn(20000)
